package br.edu.up.exemplosqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = openOrCreateDatabase("banco.db", Context.MODE_PRIVATE, null);


        String sqlDeCriacao = "CREATE TABLE IF NOT EXISTS pessoas (" +
                " id integer primary key autoincrement, " +
                " nome text," +
                " sobrenome text, " +
                " data text" +
                ")";

        db.execSQL(sqlDeCriacao);

    }

    public void gravarPessoa(View view) {

        String sqlDeInclusao = "INSERT INTO pessoas (nome, sobrenome, data) " +
                "VALUES(?,?,?)";

        EditText ed1 = (EditText) findViewById(R.id.txtNome);
        EditText ed2 = (EditText) findViewById(R.id.txtSobrenome);
        EditText ed3 = (EditText) findViewById(R.id.txtData);

        String nome = ed1.getText().toString();
        String sobrenome = ed2.getText().toString();
        String data = ed3.getText().toString();

        String[] valores = {nome, sobrenome, data};

        db.execSQL(sqlDeInclusao, valores);

        String sqlDeConsulta = "select * from pessoas where nome like ? ";

        Cursor c  = db.rawQuery(sqlDeConsulta, new String[]{"Paulo"});

        if (c.moveToNext()){

            String txtSobrenome = c.getString(2);
            Toast.makeText(this, txtSobrenome, Toast.LENGTH_LONG).show();

        }
        //Toast.makeText(this, "Sucesso!", Toast.LENGTH_LONG).show();

    }
}
